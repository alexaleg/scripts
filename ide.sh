#!/bin/bash
file=$1
type=${file#*.}
case "$type" in
	jl)
		tmux new-session -d "nvim $file" \; split-window -h 'julia'  \; attach\; select-pane -t 0
		;;
	py)
		tmux new-session -d "nvim $file" \; split-window -h 'python3'  \; attach\; select-pane -t 0
		;;
	*) 
		tmux new-session -d "nvim $file" \; split-window -h \; attach\; select-pane -t 0
	;;
esac
