#!/bin/sh
############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "Script to add file templates"
   echo
   echo "Syntax: templates [-b|h|l|c] FOLDER_NAME"
   echo "options:"
   echo "b    Intializes a beamer project."
   echo "p    Intializes a problem set project."
   echo "l    Intializes a letter project."
   echo "h     Print this Help."
   echo
}

############################################################
############################################################
# Main program                                             #
############################################################
############################################################

# Process input options.

while getopts ":hblpc:" option; do
   case $option in
      b) folder="beamer";;
      l) folder="letter";;
      p) folder="problemset";;
      h) Help
         exit;;
      \?) # Invalid option
         echo "Error: Invalid option"
         exit;;
   esac
done
name=$2
original="/home/alexander/templates/$folder"
directoryx="$(dirname -- $(readlink -fn -- "$0"; echo x))"
directory="${directoryx%x}"
echo "Creating $folder from $original to ./$name "
cp -r $original/ ./$name/
